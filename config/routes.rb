Rails.application.routes.draw do

  get 'conceptos/new'
  get 'sessions/new'
  get 'users/new'

  root 'static_pages#home'
  get  'ayuda' => 'static_pages#ayuda'
  get  'acercade' => 'static_pages#acercade'
  get  'contacto' => 'static_pages#contacto'
  get  'registro' => 'users#new'
  get  'autor' => 'static_pages#autor'
  get  'enlaces' => 'static_pages#enlaces'
  get  'tutor' => 'tutor#index'
  get 'login'   => 'sessions#new'
  get 'contenidos'   => 'conceptos#index'
  get 'concepto'   => 'tutor#show'
  get 'agregar_pregunta'   => 'quizes#agregar_pregunta'
  get 'agregar_respuesta'   => 'quizes#agregar_respuesta'
  post 'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'

  resources :users
  resources :conceptos
  resources :explicaciones do
    post :preview, on: :collection
  end
  resources :preguntas
  resources :respuestas
  resources :quizes do
    resource :ejercicio
    patch :evaluar, on: :member
  end

  resources :conocimientos do
    get :html, on: :collection
  end

  resources :lecciones, only: [:index, :show] do
    get 'crear/:concepto_id/:red_id', to: 'lecciones#crear', on: :collection
    resources :parte, only: [:show]
  end

  resources :conceptos do
    resources :comentarios, :only => [:create]
  end
end
