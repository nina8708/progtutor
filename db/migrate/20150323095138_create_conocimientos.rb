class CreateConocimientos < ActiveRecord::Migration
  def change
    create_table :conocimientos do |t|
      t.integer :user_id
      t.integer :concepto_id
      t.boolean :conocido
      t.timestamps null: false
    end
  end
end
