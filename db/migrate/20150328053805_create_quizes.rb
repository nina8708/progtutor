class CreateQuizes < ActiveRecord::Migration
  def change
    create_table :quizes do |t|
      t.string :nombre

      t.timestamps null: false
    end
  end
end
