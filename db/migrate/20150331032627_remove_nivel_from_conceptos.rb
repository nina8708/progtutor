class RemoveNivelFromConceptos < ActiveRecord::Migration
  def change
    remove_column :conceptos, :nivel, :integer
  end
end
