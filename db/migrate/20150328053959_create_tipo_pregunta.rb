class CreateTipoPregunta < ActiveRecord::Migration
  def change
    create_table :tipos_preguntas do |t|
      t.string :nombre
      t.string :explicacion

      t.timestamps null: false
    end
  end
end
