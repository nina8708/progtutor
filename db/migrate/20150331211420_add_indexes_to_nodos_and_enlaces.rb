class AddIndexesToNodosAndEnlaces < ActiveRecord::Migration
  def change
    add_index :nodos, :concepto_id
    add_index :enlaces, :padre_id
    add_index :enlaces, :hijo_id
  end
end
