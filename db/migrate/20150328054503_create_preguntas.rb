class CreatePreguntas < ActiveRecord::Migration
  def change
    create_table :preguntas do |t|
      t.integer :quiz_id
      t.integer :tipo_pregunta_id
      t.integer :concepto_id
      t.string :pregunta

      t.timestamps null: false
    end
  end
end
