class CreateRespuestas < ActiveRecord::Migration
  def change
    create_table :respuestas do |t|
      t.integer :pregunta_id
      t.string :respuesta
      t.integer :puntuacion

      t.timestamps null: false
    end
  end
end
