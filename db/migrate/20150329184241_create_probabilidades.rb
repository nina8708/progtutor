class CreateProbabilidades < ActiveRecord::Migration
  def change
    create_table :probabilidades do |t|
      t.integer :concepto_id
      t.string :probabilidad
      t.integer :red_id

      t.timestamps null: false
    end
  end
end
