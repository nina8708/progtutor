class ChangeEnlacePadreAndHijoColumns < ActiveRecord::Migration
  def change
  	rename_column :enlaces, :padre, :padre_id
  	add_index :enlaces, :padre_id
  	rename_column :enlaces, :hijo, :hijo_id
  	add_index :enlaces, :hijo_id
  end
end
