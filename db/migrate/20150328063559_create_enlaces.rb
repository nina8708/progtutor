class CreateEnlaces < ActiveRecord::Migration
  def change
    create_table :enlaces do |t|
      t.integer :padre
      t.integer :hijo
      t.timestamps null: false
    end
  end
end
