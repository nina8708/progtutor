class CreateComentarios < ActiveRecord::Migration
  def change
    create_table :comentarios do |t|
      t.belongs_to :user
      t.belongs_to :concepto
      t.string :mensaje
      t.boolean :moderado, default: false
      t.timestamps null: false
    end
  end
end
