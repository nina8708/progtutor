class AddRoleToUsers < ActiveRecord::Migration
  def change
    remove_column :users, :admin, :boolean
    add_column :users, :rol_id, :integer, default: 2
  end
end
