class MoveConceptoFromPreguntaToQuiz < ActiveRecord::Migration
  def change
    remove_column :preguntas, :concepto_id, :integer
    add_column :quizes, :concepto_id, :integer
  end
end
