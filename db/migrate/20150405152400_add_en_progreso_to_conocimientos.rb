class AddEnProgresoToConocimientos < ActiveRecord::Migration
  def change
    add_column :conocimientos, :procesando, :boolean, default: false
  end
end
