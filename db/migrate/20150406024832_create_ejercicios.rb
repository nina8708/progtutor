class CreateEjercicios < ActiveRecord::Migration
  def change
    create_table :ejercicios do |t|
      t.belongs_to :quiz
      t.string :enunciado
      t.string :codigo_evaluador
      t.integer :puntuacion
      t.timestamps null: false
    end
  end
end
