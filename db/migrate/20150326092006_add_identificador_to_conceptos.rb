class AddIdentificadorToConceptos < ActiveRecord::Migration
  def change
    add_column :conceptos, :identificador, :string
  end
end
