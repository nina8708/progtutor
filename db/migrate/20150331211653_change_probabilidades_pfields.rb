class ChangeProbabilidadesPfields < ActiveRecord::Migration
  def change
    rename_column :probabilidades, :p, :pv
    add_column :probabilidades, :pf, :float
  end
end
