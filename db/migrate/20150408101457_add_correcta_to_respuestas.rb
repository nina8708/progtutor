class AddCorrectaToRespuestas < ActiveRecord::Migration
  def change
    add_column :respuestas, :correcta, :boolean
  end
end
