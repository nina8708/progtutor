class EliminarConceptosRedes < ActiveRecord::Migration
  def change
    drop_table :conceptos_redes
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
