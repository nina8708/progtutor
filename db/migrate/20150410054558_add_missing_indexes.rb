class AddMissingIndexes < ActiveRecord::Migration
  def change
    add_index :explicaciones, :concepto_id
    add_index :ejercicios, :quiz_id
    add_index :conocimientos, :user_id
    add_index :conocimientos, :concepto_id
    add_index :conocimientos, [:user_id, :concepto_id]
    add_index :comentarios, :concepto_id
    add_index :comentarios, :user_id
    add_index :users, :rol_id
    add_index :redes, :concepto_base_id
    add_index :partes, :leccion_id
    add_index :partes, :concepto_id
    add_index :partes, [:concepto_id, :leccion_id]
    add_index :nodos, :red_id
    add_index :lecciones, :user_id
    add_index :lecciones, :concepto_id
    add_index :lecciones, :red_id
    add_index :preguntas, :quiz_id
    add_index :preguntas, :tipo_pregunta_id
    add_index :quizes, :concepto_id
    add_index :respuestas, :pregunta_id
  end
end
