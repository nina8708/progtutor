class CreateExplicaciones < ActiveRecord::Migration
  def change
    create_table :explicaciones do |t|
      t.integer :concepto_id
      t.text :explicacion

      t.timestamps null: false
    end
  end
end
