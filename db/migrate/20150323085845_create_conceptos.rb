class CreateConceptos < ActiveRecord::Migration
  def change
    create_table :conceptos do |t|
      t.string :nombre
      t.integer :nivel
      t.string :identificador
      t.timestamps null: false
    end
  end
end
