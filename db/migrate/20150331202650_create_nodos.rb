class CreateNodos < ActiveRecord::Migration
  def change
    create_table :nodos do |t|
      t.references :concepto
      t.references :red
      t.integer :nivel
      t.timestamps null: false
    end
  end
end
