class MovePuntajeToPreguntas < ActiveRecord::Migration
  def change
    remove_column :respuestas, :puntuacion, :integer
    add_column :preguntas, :puntuacion, :integer
  end
end
