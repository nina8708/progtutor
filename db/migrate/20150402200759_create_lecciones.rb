class CreateLecciones < ActiveRecord::Migration
  def change
    create_table :lecciones do |t|
      t.belongs_to :user
      t.belongs_to :concepto
      t.timestamps null: false
    end
  end
end
