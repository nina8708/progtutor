class AddRevisionToConocimientos < ActiveRecord::Migration
  def change
    add_column :conocimientos, :revision, :boolean
  end
end
