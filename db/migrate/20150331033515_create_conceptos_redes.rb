class CreateConceptosRedes < ActiveRecord::Migration
  def change
    create_table :conceptos_redes do |t|
      t.integer :concepto_id
      t.integer :nivel
      t.integer :red_id

      t.timestamps null: false
    end
  end
end