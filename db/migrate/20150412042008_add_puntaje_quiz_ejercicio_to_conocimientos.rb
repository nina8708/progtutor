class AddPuntajeQuizEjercicioToConocimientos < ActiveRecord::Migration
  def change
    add_column :conocimientos, :puntuacion_quiz, :float, default: 0
    add_column :conocimientos, :puntuacion_ejercicio, :float, default: 0
  end
end
