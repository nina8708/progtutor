class ChangeProbabilidadesFields < ActiveRecord::Migration
  def change
    remove_column :probabilidades, :concepto_id, :integer
    remove_column :probabilidades, :red_id, :integer
    remove_column :probabilidades, :probabilidad, :string
    add_column :probabilidades, :nodo_id, :integer
    add_index :probabilidades, :nodo_id
    add_column :probabilidades, :i, :integer
    add_column :probabilidades, :p, :float
  end
end
