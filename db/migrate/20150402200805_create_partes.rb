class CreatePartes < ActiveRecord::Migration
  def change
    create_table :partes do |t|
      t.belongs_to :leccion
      t.belongs_to :concepto
      t.integer :numero
      t.timestamps null: false
    end
  end
end
