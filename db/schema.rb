# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150412042008) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "comentarios", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "concepto_id"
    t.string   "mensaje"
    t.boolean  "moderado",    default: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "comentarios", ["concepto_id"], name: "index_comentarios_on_concepto_id", using: :btree
  add_index "comentarios", ["user_id"], name: "index_comentarios_on_user_id", using: :btree

  create_table "conceptos", force: :cascade do |t|
    t.text     "nombre"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.text     "identificador"
  end

  create_table "conocimientos", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "concepto_id"
    t.boolean  "conocido"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "intentos"
    t.boolean  "revision"
    t.boolean  "procesando",           default: false
    t.float    "puntuacion_quiz",      default: 0.0
    t.float    "puntuacion_ejercicio", default: 0.0
  end

  add_index "conocimientos", ["concepto_id"], name: "index_conocimientos_on_concepto_id", using: :btree
  add_index "conocimientos", ["user_id", "concepto_id"], name: "index_conocimientos_on_user_id_and_concepto_id", using: :btree
  add_index "conocimientos", ["user_id"], name: "index_conocimientos_on_user_id", using: :btree

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "ejercicios", force: :cascade do |t|
    t.integer  "quiz_id"
    t.string   "enunciado"
    t.string   "codigo_evaluador"
    t.integer  "puntuacion"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "ejercicios", ["quiz_id"], name: "index_ejercicios_on_quiz_id", using: :btree

  create_table "enlaces", force: :cascade do |t|
    t.integer  "padre_id"
    t.integer  "hijo_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "enlaces", ["hijo_id"], name: "index_enlaces_on_hijo_id", using: :btree
  add_index "enlaces", ["padre_id"], name: "index_enlaces_on_padre_id", using: :btree

  create_table "explicaciones", force: :cascade do |t|
    t.integer  "concepto_id"
    t.text     "explicacion"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "explicaciones", ["concepto_id"], name: "index_explicaciones_on_concepto_id", using: :btree

  create_table "lecciones", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "concepto_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "red_id"
  end

  add_index "lecciones", ["concepto_id"], name: "index_lecciones_on_concepto_id", using: :btree
  add_index "lecciones", ["red_id"], name: "index_lecciones_on_red_id", using: :btree
  add_index "lecciones", ["user_id"], name: "index_lecciones_on_user_id", using: :btree

  create_table "nodos", force: :cascade do |t|
    t.integer  "concepto_id"
    t.integer  "red_id"
    t.integer  "nivel"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "nodos", ["concepto_id"], name: "index_nodos_on_concepto_id", using: :btree
  add_index "nodos", ["red_id"], name: "index_nodos_on_red_id", using: :btree

  create_table "partes", force: :cascade do |t|
    t.integer  "leccion_id"
    t.integer  "concepto_id"
    t.integer  "numero"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "partes", ["concepto_id", "leccion_id"], name: "index_partes_on_concepto_id_and_leccion_id", using: :btree
  add_index "partes", ["concepto_id"], name: "index_partes_on_concepto_id", using: :btree
  add_index "partes", ["leccion_id"], name: "index_partes_on_leccion_id", using: :btree

  create_table "preguntas", force: :cascade do |t|
    t.integer  "quiz_id"
    t.integer  "tipo_pregunta_id"
    t.text     "pregunta"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "puntuacion"
  end

  add_index "preguntas", ["quiz_id"], name: "index_preguntas_on_quiz_id", using: :btree
  add_index "preguntas", ["tipo_pregunta_id"], name: "index_preguntas_on_tipo_pregunta_id", using: :btree

  create_table "probabilidades", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "nodo_id"
    t.integer  "i"
    t.float    "pv"
    t.float    "pf"
  end

  add_index "probabilidades", ["nodo_id"], name: "index_probabilidades_on_nodo_id", using: :btree

  create_table "quizes", force: :cascade do |t|
    t.text     "nombre"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "concepto_id"
  end

  add_index "quizes", ["concepto_id"], name: "index_quizes_on_concepto_id", using: :btree

  create_table "redes", force: :cascade do |t|
    t.text     "nombre"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "concepto_base_id"
  end

  add_index "redes", ["concepto_base_id"], name: "index_redes_on_concepto_base_id", using: :btree

  create_table "respuestas", force: :cascade do |t|
    t.integer  "pregunta_id"
    t.text     "respuesta"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.boolean  "correcta"
  end

  add_index "respuestas", ["pregunta_id"], name: "index_respuestas_on_pregunta_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "descripcion"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "tipos_preguntas", force: :cascade do |t|
    t.text     "nombre"
    t.text     "explicacion"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "users", force: :cascade do |t|
    t.text     "name"
    t.text     "email"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.text     "password_digest"
    t.text     "remember_digest"
    t.integer  "rol_id",          default: 2
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["rol_id"], name: "index_users_on_rol_id", using: :btree

end
