module ConocimientosHelper
  def conocimiento_tag(conocimiento, red_id)
    data = {}
    if conocimiento.procesando
      clase = 'conocimiento-procesando'
      icono = 'fa-cog fa-spin'
      url = '#'
      data = {concepto_id: conocimiento.concepto.id}
    elsif conocimiento.revision && conocimiento.conocido == true
      clase = 'faa-float animated conocimiento-a-repasar'
      icono = 'fa-check'
      url = concepto_path(id: conocimiento.concepto_id)
    elsif conocimiento.revision && conocimiento.conocido == false
      clase = 'conocimiento-fallo'
      icono = 'fa-exclamation'
      url = concepto_path(id: conocimiento.concepto_id)
    elsif !conocimiento.revision && conocimiento.conocido == true
      clase = 'conocimiento-aprendido'
      icono = 'fa-check'
      url = concepto_path(id: conocimiento.concepto_id)
    elsif !conocimiento.revision && conocimiento.conocido == false
      clase = 'conocimiento-desbloqueado'
      icono = 'fa-unlock'
      url = concepto_path(id: conocimiento.concepto_id)
    else
      clase = 'conocimiento-bloqueado'
      icono = 'fa-lock'
      url = '#leccion-modal'
      data = {toggle: "modal", concepto_id: conocimiento.concepto.id, red_id: red_id}
    end

    link_to(url, id: conocimiento.id, class: 'centered-text center-block', data: data) do
      content_tag(:span, '', class: "fa-stack fa-4x #{clase}") do
        content_tag(:i, '', class: "fa fa-circle-thin fa-stack-2x") +
        content_tag(:i, '', class: "fa #{icono} fa-stack-1x")
      end
    end
  end
end
