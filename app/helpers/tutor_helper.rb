module TutorHelper
  def mostrar_progreso_nivel(nivel, porcentaje)
    content_tag(:h1, "Nivel #{nivel}") +
    content_tag(:div, '', class: 'progress center-block contenedor-progreso') do
      content_tag(:div, '', class: 'progress-bar', style: "width:#{porcentaje}%")
    end +
    content_tag(:h2, "#{porcentaje}% completado")
  end
end
