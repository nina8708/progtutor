class PreguntasController < ApplicationController
  def create
    pregunta = Pregunta.new(pregunta_params)
     if pregunta.save
      flash[:success] = "Pregunta agregada."
      quiz = Quiz.find(pregunta.quiz_id)
      redirect_to edit_quiz_url(quiz.concepto_id)
    else
      redirect_to contenidos_path
    end
  end

  def destroy
  end

  private

    def pregunta_params
      params.require(:pregunta).permit(:quiz_id, :tipo_pregunta_id, :pregunta, :puntuacion)
    end
end
