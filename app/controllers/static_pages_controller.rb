class StaticPagesController < ApplicationController

  def home
    if logged_in?
      redirect_to tutor_path
    end
  end

  def ayuda
  end

  def acercade
  end

  def contacto
  end

  def registro
  end

  def login
  end

  def autor
  end

  def enlaces
  end
end
