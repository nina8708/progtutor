class ConocimientosController < ApplicationController

  def html
    # TODO Soporte multi-red
    @red_id = 1
    @conocimientos = Conocimiento
      .includes(:concepto)
      .where(id: params[:ids])
    respond_to do |format|
      format.json
    end
  end

end
