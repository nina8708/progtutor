class ParteController < ApplicationController
  before_action :set_parte_and_leccion, :check_parte_and_leccion

  def show
  end

  private
    def set_parte_and_leccion
      @parte = Parte.includes({concepto: {}, leccion: {concepto: {}}}).find(params[:id])
      @leccion = @parte.leccion
    end

    def check_parte_and_leccion
      # unless @leccion.user == current_user and current_user.conoce?(@parte.concepto)
      unless @leccion.user == current_user
        redirect_to leccion_url(@leccion)
      end
    end
end
