class EjerciciosController < ApplicationController
  before_action :set_quiz
  before_action :set_ejercicio, only: [:edit, :update]

  def new
  end

  def create
    @ejercicio = Ejercicio.new(ejercicio_params)
    @ejercicio.quiz = @quiz
    @ejercicio.save
    redirect_to contenidos_url
  end

  def edit
  end

  def update
    @ejercicio.update_attributes(ejercicio_params)
    redirect_to contenidos_url
  end

  private

  def set_quiz
    @quiz = Quiz.find_by_id(params[:quiz_id])
  end

  def set_ejercicio
    @ejercicio = @quiz.ejercicio
  end

  def ejercicio_params
    params.require(:ejercicio).permit(:enunciado, :codigo_evaluador, :puntuacion)
  end
end
