class ComentariosController < ApplicationController
  def create
    @concepto = Concepto.find(params[:concepto_id])
    @comentario = @concepto.comentarios.new(comentario_params)
    @comentario.user = current_user
    @comentario.save
    redirect_to concepto_path(id: @concepto.id)
  end

  private

  def comentario_params
    params.require(:comentario).permit(:mensaje)
  end
end
