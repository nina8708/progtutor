class ExplicacionesController < ApplicationController

  def show
  end

  def edit
    @explicacion = Explicacion.find_by_concepto_id(params[:id])
    @concepto = Concepto.find_by_id(params[:id])
    if @explicacion.nil?
      @explicacion = Explicacion.new
      @explicacion.concepto_id = @concepto.id
      @explicacion.save
    end
  end

  def new
    @explicacion = Explicacion.new
  end

  def create
    @explicacion = Explicacion.new(explicacion_params)
    if @explicacion.save
      flash[:success] = "Datos actualizados."
      redirect_to contenidos_path
    else
      render 'edit'
    end
  end

  def index
  end
  
  def update
    @explicacion = Explicacion.find(params[:id])
    if @explicacion.update_attributes(explicacion_params)
      flash[:success] = "Datos actualizados."
      redirect_to contenidos_path
    else
      render 'edit'
    end
  end

  def destroy
  end

  def preview
    @data = Markitup::Rails.configuration.formatter.call(params[:data])
    respond_to do |format|
      format.html { render :preview, layout: 'markitup' }
    end
  end

  private

    def explicacion_params
      params.require(:explicacion).permit(:explicacion)
    end
end
