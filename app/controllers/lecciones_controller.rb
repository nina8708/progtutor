class LeccionesController < ApplicationController
  before_action :set_leccion, only: :show
  def index
    @lecciones = current_user.lecciones.includes({concepto: {}, partes: {concepto: {}}})
  end

  def show
    @red = Red.includes({nodos: {concepto: {}}}).find_by_id(@leccion.red_id)
    @estado_conocimientos = current_user.estado_conocimientos(@red, @leccion.partes.pluck('concepto_id'))
    redirect_to :index unless @leccion.user == current_user
  end

  def crear
    leccion = Leccion.crear(params[:concepto_id], params[:red_id], current_user)
    redirect_to leccion_path(leccion)
  end

  private
    def set_leccion
      @leccion = Leccion.includes({partes: {concepto: {}}}).find(params[:id])
    end
end
