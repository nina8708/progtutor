class ConceptosController < ApplicationController
  before_action :set_concepto, only: [:show, :edit, :update, :destroy]

  def index
    @conceptos = Concepto.paginate(page: params[:page], :per_page => 10)
  end

  def show
  end

  def new
    @concepto = Concepto.new
  end

  def edit
  end

  def create
    @concepto = Concepto.new(concepto_params)

    respond_to do |format|
      if @concepto.save
        format.html { redirect_to @concepto, notice: 'El concepto fue creado.' }
        format.json { render :show, status: :created, location: @concepto }
      else
        format.html { render :new }
        format.json { render json: @concepto.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @concepto.update(concepto_params)
        format.html { redirect_to @concepto, notice: 'El concepto fue actualizado.' }
        format.json { render :show, status: :ok, location: @concepto }
      else
        format.html { render :edit }
        format.json { render json: @concepto.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @concepto.destroy
    respond_to do |format|
      format.html { redirect_to conceptos_url, notice: 'El concepto fue destruído.' }
      format.json { head :no_content }
    end
  end

  private

    def set_concepto
      @concepto = Concepto.find(params[:id])
    end

    def concepto_params
      params.require(:concepto).permit(:nombre, :nivel)
    end
end
