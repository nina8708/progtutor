class TutorController < ApplicationController

  def index
    # TODO Soporte multi-red
    @red = Red.find_by_id(1)
    @progreso = current_user.progreso_en_red(@red)
  end

  def show
    @concepto = Concepto.completo.find_by_id(params[:id])
    @comentario = Comentario.new
  end

end
