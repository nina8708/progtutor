class QuizesController < ApplicationController

  def edit
    @i = 1
    @concepto = Concepto.find_by_id(params[:id])
    @quiz = Quiz.find_by_concepto_id(params[:id])
    @tipos_preguntas = TipoPregunta.all
    @tipos_preguntas_array = @tipos_preguntas.map { |tp| [ tp.nombre, tp.id ] }
    if @quiz.nil?
      @quiz = Quiz.new
      @quiz.concepto_id = @concepto.id
      @quiz.save
    else
      @ṕreguntas = Pregunta.where(quiz_id: @quiz.id)
    end
  end

  def new
    @quiz = Quiz.new
  end

  def create
    @quiz = Quiz.new(private_quiz_params)
    if @quiz.save
      flash[:success] = "Datos actualizados."
      redirect_to contenidos_path
    else
      render 'edit'
    end
  end

  def update
    @quiz = Quiz.find(params[:id])
    if @quiz.update_attributes(private_quiz_params)
      flash[:success] = "Datos actualizados."
      redirect_to contenidos_path
    else
      render 'edit'
    end
  end

  def agregar_respuesta
    @index = params[:pregunta][:i]
    @pregunta = Pregunta.find(params[:pregunta][:pregunta_id])
    respond_to do |format|
      format.js
    end
  end

  def agregar_pregunta
    @tipos_preguntas = TipoPregunta.all
    @tipos_preguntas_array = @tipos_preguntas.map { |tp| [ tp.nombre, tp.id ] }
    @quiz = Quiz.find(params[:quiz][:quiz_id])
    respond_to do |format|
      format.js
    end
  end
  
  def evaluar
    # TODO Soporte multi-red
    @red = Red.find_by_id(1)
    @quiz = Quiz.completo.find_by_id(params[:id])
    @resultados = @quiz.evaluar(quiz_params, current_user, @red)
  end

  def quiz_params
    params.require(:quiz).permit(
      preguntas_attributes: [
        :id,
        :seleccionada,
        :pregunta,
        respuestas_attributes: [
          :id,
          :seleccionada,
          :respuesta
        ]
      ]
    )
  end

    private

    def private_quiz_params
      params.require(:quiz).permit(
        :nombre, preguntas_attributes: [
          :id, :pregunta, :tipo_pregunta_id, :puntuacion, respuestas_attributes: [
            :id, :respuesta, :correcta]])
    end
end
