class RespuestasController < ApplicationController

  def create
    respuesta = Respuesta.new(respuesta_params)
    respuesta.save
    pregunta = Pregunta.find(respuesta.pregunta_id)
    quiz = Quiz.find(pregunta.quiz_id)
    flash[:success] = "Respuesta agregada."
    redirect_to edit_quiz_url(quiz.concepto_id)
  end

  def destroy
    respuesta = Respuesta.find(params[:id])
    pregunta = Pregunta.find(respuesta.pregunta_id)
    respuesta.destroy
    flash[:success] = "Respuesta eliminada."
    redirect_to edit_quiz_url(pregunta.quiz_id) 
  end

  private

    def respuesta_params
      params.require(:respuesta).permit(:id, :pregunta_id, :respuesta, :correcta)
    end
end
