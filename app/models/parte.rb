class Parte < ActiveRecord::Base
  belongs_to :leccion
  belongs_to :concepto

  def anterior
    @anterior ||= Parte
      .includes(:concepto)
      .where(leccion: leccion, numero: numero.pred)
      .first
  end

  def siguiente
    @siguiente ||= Parte
      .includes(:concepto)
      .where(leccion: leccion, numero: numero.succ)
      .first
  end
end
