class Pregunta < ActiveRecord::Base
  belongs_to :quiz
  belongs_to :tipo_pregunta
  has_many :respuestas
  attr_accessor :seleccionada
  accepts_nested_attributes_for :respuestas, :reject_if => lambda { |a| a[:respuesta].blank? }
end
