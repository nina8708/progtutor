class Conocimiento < ActiveRecord::Base
  belongs_to :user
  belongs_to :concepto

  INTENTOS_MAXIMOS = 3

  def actualizar(params, user, red)
    assign_attributes(params)
    if conocido_changed?
      if self.conocido
        marcar_hijos(red)
        user.actualizar_conocimientos(red, self.concepto_id)
      end
    end
    self.revision = !self.conocido && (self.concepto != red.concepto_base)
    if self.revision
      self.intentos += 1
      if self.intentos >= INTENTOS_MAXIMOS
        marcar_padres(red)
        user.repasar_conceptos(red, self.concepto_id)
        self.intentos = 0
      end
    end
    save
  end

  private

  def marcar_hijos(red)
    marcar(
      red
        .nodos
        .find_by_concepto_id(self.concepto_id)
        .hijos
        .pluck(:id)
    )
  end

  def marcar_padres(red)
    marcar(
      red
        .nodos
        .find_by_concepto_id(self.concepto_id)
        .padres
        .pluck(:id)
    )
  end

  def marcar(ids_conceptos)
    Conocimiento
      .where(concepto_id: ids_conceptos)
      .update_all(procesando: true)
  end
end
