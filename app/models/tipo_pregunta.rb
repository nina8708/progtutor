class TipoPregunta < ActiveRecord::Base
  has_many :preguntas

  SIMPLE = 1
  MULTIPLE = 3

  TIPOS = {
    SIMPLE => "Selección simple",
    MULTIPLE => "Selección múltiple",
  }
end
