class Enlace < ActiveRecord::Base
  belongs_to :padre, class_name: "Nodo"
  belongs_to :hijo, class_name: "Nodo"
end
