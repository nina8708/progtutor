class Red < ActiveRecord::Base
  has_many :nodos
  has_many :conceptos, through: :nodos
  has_many :lecciones

  belongs_to :concepto_base, class_name: 'Concepto'

  default_scope { includes(:conceptos, nodos: [:padres, :hijos]) }

  def inicializar
    @sbn = Sbn::Net.new(nombre)
    variables = conceptos.each_with_object({}) do |concepto, variables|
      simbolo = concepto.identificador.to_sym
      nodo_id = concepto.nodo.id
      probabilidades = Probabilidad
        .where(nodo_id: nodo_id)
        .order(:i)
        .pluck(:pv, :pf)
        .flatten
      variables[nodo_id] = Sbn::Variable.new(@sbn, simbolo, probabilidades)
    end
    nodos.each do |nodo|
      variable = variables[nodo.id]
      nodo.hijos.each do |hijo|
        variable.add_child(variables[hijo.id])
      end
    end
  end

  def consultar(variable, evidencia)
    inicializar if @sbn.nil?
    @sbn.set_evidence(evidencia)
    @sbn.query_variable(variable)
  end
end
