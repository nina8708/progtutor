class User < ActiveRecord::Base
  attr_accessor :remember_token
  before_save { self.email = email.downcase }
  
  validates :name, presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
            format: { with: VALID_EMAIL_REGEX }, 
            uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password, length: { minimum: 6 }, allow_blank: true

  has_many :conocimientos, dependent: :destroy
  has_many :lecciones, dependent: :destroy
  has_many :comentarios, dependent: :destroy
  belongs_to :rol

  after_create :crear_curso

  PROBABILIDAD_MINIMA = 0.6

  def admin?
    self.rol_id == 1
  end

  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  def User.new_token
    SecureRandom.urlsafe_base64
  end

  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  def authenticated?(remember_token)
    return false if remember_digest.nil?
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end

  def forget
    update_attribute(:remember_digest, nil)
  end

  def progreso_en_red(red)
    max_nivel = red.nodos.maximum(:nivel)
    1.upto(max_nivel).map do |nivel|
      conceptos_nivel = red.conceptos.where({nodos: {nivel: nivel}})
      n_conceptos_nivel = conceptos_nivel.count
      conceptos_conocidos_nivel = conocimientos
        .includes(:concepto)
        .where(concepto: conceptos_nivel)

      n_conceptos_conocidos_nivel = conocimientos
        .joins(:concepto)
        .where(concepto: conceptos_nivel)
        .where(conocido: true).count
      {
        nivel: nivel,
        progreso: (n_conceptos_conocidos_nivel.to_f / n_conceptos_nivel * 100).round(),
        conocimientos: conceptos_conocidos_nivel
      }
    end
  end

  def estado_conocimientos(red, partes)
    conceptos_red = red.conceptos
    conceptos_conocidos = conocimientos
      .includes(:concepto)
      .where(concepto: partes)
  end

  def conoce?(concepto)
    conocimientos
      .where(concepto: concepto)
      .where(conocido: true)
      .any?
  end

  def crear_curso
    transaction do
      Concepto.all.pluck(:id).each_with_index do |concepto_id, index|
        index == 0 ? conocido = index : conocido = nil
        conocimientos.create! concepto_id: concepto_id, conocido: conocido, intentos: 0, revision: false
      end
    end
  end

  def actualizar_conocimientos(red, concepto_id)
    ids_conocidos = conocimientos.where(conocido: true).pluck('concepto_id')
    evidencia = generar_evidencia()

    nodo_a_consultar = red.nodos.find_by_concepto_id(concepto_id)
    hijos_nodo = nodo_a_consultar.hijos
    ids_hijos_nodo = hijos_nodo.pluck('concepto_id')

    if (ids_hijos_nodo - ids_conocidos).any?
      hijos_nodo.each do |hijo|
        if !(ids_conocidos.include? hijo.concepto_id)
          concepto_hijo = Concepto.find_by_id(hijo.concepto_id)
          conocimiento = conocimientos.find_by_concepto_id(concepto_hijo.id)
          # Verifica si todos los conceptos padres son conocidos
          conceptos_padres_desconocidos = Conocimiento
            .where.not(conocido: true)
            .where(concepto_id: hijo.padres.pluck(:concepto_id))
          if conceptos_padres_desconocidos.empty?
            Delayed::Worker.logger.debug("#{concepto_hijo.nombre} desbloqueado por pre-requisitos completos")
            conocimiento.conocido = false
          else
            Delayed::Worker.logger.debug("Infiriendo #{concepto_hijo.nombre} por Bayes")
            padres_hijo = hijo.padres
            ids_padres_hijo = padres_hijo.pluck('concepto_id')
            padres_hijo.each do |padre|
              evidencia_identificador = Concepto.find_by_id(padre.concepto_id).identificador.to_sym
              (ids_conocidos.include? padre.concepto_id) ? (valor = :true) : (valor = :false)
              evidencia[evidencia_identificador] = valor
            end
            consulta_identificador = red.conceptos.find_by_id(hijo.concepto_id).identificador.to_sym
            consulta = red.consultar(consulta_identificador, evidencia)[:true]

            if consulta > PROBABILIDAD_MINIMA
              Delayed::Worker.logger.debug("#{concepto_hijo.nombre} desbloqueado por Bayes")
              conocimiento.conocido = false
            end
          end
          conocimiento.procesando = false
          conocimiento.save
        end
      end
    end
  end
  handle_asynchronously :actualizar_conocimientos

  def repasar_conceptos(red, concepto_id)
    concepto_ident = Concepto.find_by_id(concepto_id).identificador
    evidencia = {concepto_ident.to_sym => :false}

    conceptos_padres = red
      .nodos
      .find_by_concepto_id(concepto_id)
      .padres
      .joins(:concepto)
      .pluck('conceptos.id, conceptos.identificador')
      .each_with_object({}) do |concepto, conceptos_padres|
        conceptos_padres[concepto[0]] = concepto[1].to_sym
      end

    return nil if conceptos_padres.empty?

    prob_padres = {}
    conceptos_padres.each do |concepto_id, concepto_ident|
      prob_padres[concepto_id] = red.consultar(concepto_ident, evidencia)[:false]
    end

    concepto_probable_id = prob_padres.max_by {|cid, p| p}[0]
    conceptos_no_probables_ids = prob_padres.keys - [concepto_probable_id]

    conocimientos
      .where(concepto_id: concepto_probable_id)
      .update_all({revision: true, procesando: false})
    conocimientos
      .where(concepto_id: conceptos_no_probables_ids)
      .update_all({revision: false, procesando: false})
  end
  handle_asynchronously :repasar_conceptos

  private

  def generar_evidencia
    conocimientos
      .joins(:concepto)
      .where.not(conocido: nil)
      .pluck('conceptos.identificador', 'conocimientos.conocido')
      .each_with_object({}) do |conocimiento, evidencia|
        evidencia[conocimiento[0].to_sym] = conocimiento[1] ? :true : :false
      end
  end
end
