class Concepto < ActiveRecord::Base
  has_one :explicacion
  has_many :conocimientos
  has_many :preguntas
  has_many :probabilidades
  has_one :nodo
  has_one :quiz
  has_many :redes, through: :nodos
  has_many :partes
  has_many :comentarios

  scope :completo, -> {
    includes({
      explicacion: {},
      comentarios: {user: {}},
      quiz: {
        preguntas: {tipo_pregunta: {}},
        ejercicio: {}
      }
    })
  }
end
