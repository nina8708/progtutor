class Nodo < ActiveRecord::Base
  belongs_to :red
  belongs_to :concepto
  has_many :probabilidades

  has_many :enlaces_hijos, foreign_key: 'padre_id', class_name: 'Enlace'
  has_many :hijos, through: :enlaces_hijos

  has_many :enlaces_padres, foreign_key: 'hijo_id', class_name: 'Enlace'
  has_many :padres, through: :enlaces_padres
end
