class Leccion < ActiveRecord::Base
  belongs_to :user
  belongs_to :concepto
  belongs_to :red
  has_many :partes, dependent: :destroy
  has_many :conceptos, through: :partes

  def self.crear(concepto_id, red_id, current_user)
    leccion = current_user
      .lecciones
      .where(concepto_id: concepto_id, red_id: red_id)
      .first_or_initialize
    if leccion.new_record?
      leccion.concepto_id = concepto_id
      leccion.red_id = red_id
      red = Red.includes({nodos: {concepto: {}}}).find_by_id(red_id)
      nodo_a_consultar = red.nodos.find_by_concepto_id(concepto_id)
      ids_conocidos = current_user.conocimientos.where(conocido: true).pluck('concepto_id')
      incluidos = Array.new << concepto_id.to_i
      encontrar_padres(nodo_a_consultar, incluidos)
      conceptos_leccion =  (incluidos.uniq.sort) - ids_conocidos
      transaction do
        leccion.save
        conceptos_leccion.each_with_index do | concepto_leccion, index |
          leccion.partes.create concepto_id: concepto_leccion, numero: index + 1
        end
      end
    end
    leccion
  end

  def self.encontrar_padres(nodo_a_consultar, incluidos)
    padres_nodo = nodo_a_consultar.padres
    padres_nodo.each do | padre |
      incluidos << padre.concepto_id
      encontrar_padres(padre, incluidos)
    end
  end

  def progreso
    (user.conocimientos.where(conocido: true, concepto: conceptos).count / partes.count.to_f * 100).round
  end
end
