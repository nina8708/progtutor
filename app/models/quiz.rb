class Quiz < ActiveRecord::Base
  belongs_to :concepto
  has_many :preguntas
  has_one :ejercicio
  accepts_nested_attributes_for :preguntas, :reject_if => lambda { |a| a[:pregunta].blank? }

  scope :completo, -> { includes({preguntas: {respuestas: {}}}) }

  MINIMA = 0.8 # Pudiera añadirse como atributo para cada quiz

  def evaluar(params, user, red)
    assign_attributes(params)

    puntuacion_maxima = preguntas.sum(:puntuacion)

    puntuacion_obtenida = 0
    preguntas.each do |pregunta|
      if pregunta.tipo_pregunta_id == TipoPregunta::SIMPLE
        respuesta = pregunta.respuestas.find_by_id(pregunta.seleccionada)
        puntuacion_obtenida += pregunta.puntuacion if respuesta && respuesta.correcta
      elsif pregunta.tipo_pregunta_id == TipoPregunta::MULTIPLE
        total_correctas = pregunta.respuestas.where(correcta: true).count
        correctas = 0
        erradas = 0
        pregunta.respuestas.each do |respuesta|
          if respuesta.seleccionada == '1'
            if respuesta.correcta
              correctas += 1
            else
              erradas += 1
            end
          end
        end
        puntuacion_obtenida += (correctas - erradas) / total_correctas * pregunta.puntuacion
      end
    end

    puntuacion_obtenida = 0 if puntuacion_obtenida < 0
    puntuacion_porcentual = puntuacion_obtenida.to_f / puntuacion_maxima
    aprobado = puntuacion_porcentual >= MINIMA
    comentario = aprobado ? "Aprobado!" : "Reprobado!"

    conocimiento = user.conocimientos.find_by_concepto_id(self.concepto_id)
    conocimiento.actualizar({conocido: aprobado}, user, red)

    {
      nota: (puntuacion_porcentual * 100).round,
      nota_minima: MINIMA * 100,
      comentario: comentario
    }
  end
end
