ready = ->
  # Diálogo modal para conocimientos bloqueados
  $('#leccion-modal').on 'show.bs.modal', (e) ->
    concepto_id = $(e.relatedTarget).data('concepto-id')
    red_id = $(e.relatedTarget).data('red-id')
    $('#leccion-modal a').attr('href', "lecciones/crear/#{concepto_id}/#{red_id}")
    return

  # Consulta periódica de conocimientos en proceso
  clearTimeout window.conocimientos_timeout
  window.consultar_conocimientos()

  # Resaltado sintaxis python
  $('pre code').each (i, block) ->
    hljs.highlightBlock block
    return

  # Editor de explicaciones
  $(".markitup").markItUp(window.markitUpSettings)

  # Edtor de código
  yourcode = document.getElementById('yourcode')
  if yourcode
    window.editor = CodeMirror.fromTextArea(yourcode, window.codemirrorSettings)

$(document).ready ready
$(document).on 'page:load', ready
