# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

window.consultar_conocimientos = ->
  ids = $.map $('.conocimiento-procesando').parents('a'), (conocimiento) ->
    $(conocimiento).attr('id')
  $.getJSON '/conocimientos/html.json', {ids: ids}, window.actualizar_conocimientos

window.actualizar_conocimientos = (conocimientos) ->
  $.each conocimientos, ->
    $("\##{this.id}").html this.html
  if $('.conocimiento-procesando').length > 0
    window.conocimientos_timeout = setTimeout window.consultar_conocimientos, 500
