# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

window.codemirrorSettings =
  mode:
    name: 'python'
    version: 2
    singleLineStringErrors: false
  lineNumbers: true
  indentUnit: 2
  matchBrackets: true