outf = (text) ->
  mypre = document.getElementById('output')
  mypre.innerHTML = mypre.innerHTML + text
  return

builtinRead = (x) ->
  if Sk.builtinFiles == undefined or Sk.builtinFiles['files'][x] == undefined
    throw "File not found: '#{x}'"
  Sk.builtinFiles['files'][x]

window.runit = ->
  prog = window.editor.getValue() + "\n" + $('#ejercicio').data('eval-code')
  console.log prog
  mypre = document.getElementById('output')
  mypre.innerHTML = ''
  Sk.pre = 'output'
  Sk.configure
    output: outf
    read: builtinRead
  myPromise = Sk.misceval.asyncToPromise(->
    Sk.importMainWithBody '<stdin>', false, prog
  )
  myPromise.then ((mod) ->
    console.log 'success'
    # Manera arcáica de saber el resultado
    window.showResults($("#output").text().split('\n')[0] == "Excelente")
    return
  ), (err) ->
    mypre.innerHTML = err.toString()
    return
  return

setOutputColor = (color) ->
  $("#output").css({"background": color})

window.showResults = (approved) ->
  setOutputColor(if approved then "#afa" else "#faa")
